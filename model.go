package goauth

import "time"

// Account represents an Account entity
// An account is an user obtained from one social media platform, for example Facebook
type Account struct {
	ID         string    `json:"id"`
	PlatformID string    `json:"platform_id"`
	Platform   string    `json:"platform"`
	Name       string    `json:"name"`
	Email      string    `json:"email"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}

// AuthorizationCode is a temporary code used to exchange for an access/refresh token
type AuthorizationCode struct {
	ID                  string    `json:"id"`
	AccountID           string    `json:"string"`
	Code                string    `json:"code"`
	CodeChallenge       string    `json:"-"`
	CodeChallengeMethod string    `json:"-"`
	CreatedAt           time.Time `json:"created_at"`
	UpdatedAt           time.Time `json:"updated_at"`
	ConvertedToToken    string    `json:"-"`
}

// AccessToken is a time-limited token to identify a request
type AccessToken struct {
	ID        string    `json:"id"`
	AccountID string    `json:"string"`
	Code      string    `json:"code"`
	Scopes    []string  `json:"scopes"`
	ExpiresAt time.Time `json:"expires_at"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// RefreshToken is a unique and permanent token to renew an expired access token
type RefreshToken struct {
	ID            string    `json:"id"`
	AccessTokenID string    `json:"access_token_id"`
	Code          string    `json:"code"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}
