package goauth

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"io"
)

// DefaultCrypto implements Crypto interface using the default aes package
type DefaultCrypto struct{}

// always make sure that we have 32 bytes
func hashKeyTo32Bytes(key string) []byte {
	output := sha256.Sum256([]byte(key))
	return output[0:]
}

// Encrypt encrypts the input with the provided key
func (c *DefaultCrypto) Encrypt(key string, input string) (string, error) {
	block, err := aes.NewCipher(hashKeyTo32Bytes(key))
	if err != nil {
		return "", err
	}
	b := base64.StdEncoding.EncodeToString([]byte(input))
	ciphertext := make([]byte, aes.BlockSize+len(b))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
	return base64.URLEncoding.EncodeToString(ciphertext), nil
}

// Decrypt decrypts the input with the provided key
func (c *DefaultCrypto) Decrypt(key string, input string) (string, error) {
	text, err := base64.URLEncoding.DecodeString(input)

	if err != nil {
		return "", err
	}

	block, err := aes.NewCipher(hashKeyTo32Bytes(key))
	if err != nil {
		return "", err
	}

	if len(text) < aes.BlockSize {
		return "", errors.New("ciphertext too short")
	}
	iv := text[:aes.BlockSize]
	text = text[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(text, text)
	data, err := base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return "", err
	}
	return string(data), nil
}
