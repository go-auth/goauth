package goauth

import (
	"crypto/rand"
	"encoding/hex"
)

// DefaultRandomizer implements Randomizer interface
type DefaultRandomizer struct{}

// RandomString returns a random string with the specified length
func (r *DefaultRandomizer) RandomString(length int) (string, error) {
	bytes := make([]byte, length)

	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}

	return hex.EncodeToString(bytes), nil
}
