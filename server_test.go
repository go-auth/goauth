package goauth

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/tanqhnguyen/gologger"
)

type serverTestSuite struct {
	suite.Suite
	server     *Server
	randomizer *MockRandomizer
	crypto     *MockCrypto
	store      *MockStore
}

func (s *serverTestSuite) SetupSuite() {
	gin.SetMode(gin.ReleaseMode)
}

func (s *serverTestSuite) SetupTest() {
	config := ServerConfig{
		RootURL:              "http://localhost.test",
		AllowedRedirectHosts: []string{"localhost.test"},
		SessionSecret:        "123",
		JWTSecret:            "123456",
		StateSecret:          "77234",
	}

	s.randomizer = new(MockRandomizer)
	s.crypto = new(MockCrypto)
	s.store = new(MockStore)

	router := gin.New()

	store := cookie.NewStore([]byte(config.SessionSecret))

	router.Use(sessions.Sessions("goauth", store))

	s.server = &Server{
		config,
		router,
		s.store,
		gologger.NewLogrusLoggerFromEnvironment(),
		s.randomizer,
		s.crypto,
	}
}

func (s *serverTestSuite) TestVerifiers() {
	t := s.Assert()

	t.True(codeVerifiers["md5"]("123", "202CB962AC59075B964B07152D234B70"))
	t.True(codeVerifiers["sha1"]("123", "40bd001563085fc35165329ea1ff5c5ecbdbbeef"))
	t.True(codeVerifiers["sha256"]("123", "a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3"))
}

func (s *serverTestSuite) TestRefreshEndpoint() {
	t := s.Assert()
	s.server.setupRefreshEndpoint()

	accountID := "123"

	accessToken := &AccessToken{
		ID:        "access-token",
		Code:      "new-access-token",
		AccountID: accountID,
		ExpiresAt: time.Now().UTC().Add(24 * time.Hour),
	}
	refreshToken := &RefreshToken{
		ID:            "refresh-token",
		Code:          "test-refresh-token",
		AccessTokenID: accessToken.ID,
	}

	s.store.On("GetRefreshToken", refreshToken.Code).Return(refreshToken, nil).Once()
	s.store.On("RefreshAccessToken", refreshToken.Code).Return(accessToken, nil)

	jsonData := []byte(`{"refresh_token": "test-refresh-token"}`)

	res := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "/v1/token", bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json")

	s.server.router.ServeHTTP(res, req)

	t.Equal(200, res.Code)

	var data tokenResponse
	err := json.Unmarshal(res.Body.Bytes(), &data)
	t.Nil(err)

	unsignedAccessToken, err := s.server.unsignAccessToken(data.AccessToken)
	t.Nil(err)

	t.Equal(accessToken.Code, unsignedAccessToken.Code)
	t.Equal(accessToken.AccountID, unsignedAccessToken.AccountID)
	t.True(time.Now().UTC().Before(unsignedAccessToken.ExpiresAt))

	s.store.AssertExpectations(s.T())
}

type tokenResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
}

func (s *serverTestSuite) TestExchangeEndpoint() {
	t := s.Assert()

	authCode := "auth-code"
	accountID := "123"

	s.store.On("GetAuthorizationCode", authCode).Return(&AuthorizationCode{
		ID:                  "1",
		Code:                authCode,
		AccountID:           accountID,
		CodeChallenge:       "202CB962AC59075B964B07152D234B70",
		CodeChallengeMethod: "md5",
	}, nil).Once()
	accessToken := &AccessToken{
		ID:        "access-token",
		Code:      "test-access-token",
		AccountID: accountID,
		ExpiresAt: time.Now().UTC().Add(24 * time.Hour),
	}
	refreshToken := &RefreshToken{
		ID:            "refresh-token",
		Code:          "test-refresh-token",
		AccessTokenID: accessToken.ID,
	}
	s.store.On("ExchangeAuthorizationCode", mock.Anything).Return(accessToken, refreshToken, nil).Once()

	s.server.setupExchangeEndpoint()

	jsonData := []byte(`{"code": "auth-code", "verifier": "123"}`)

	res := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/v1/token", bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json")

	s.server.router.ServeHTTP(res, req)

	t.Equal(200, res.Code)

	var data tokenResponse
	err := json.Unmarshal(res.Body.Bytes(), &data)
	t.Nil(err)

	unsignedAccessToken, err := s.server.unsignAccessToken(data.AccessToken)
	t.Nil(err)

	t.Equal(accessToken.Code, unsignedAccessToken.Code)
	t.Equal(accessToken.AccountID, unsignedAccessToken.AccountID)
	t.True(time.Now().UTC().Before(unsignedAccessToken.ExpiresAt))

	s.store.AssertExpectations(s.T())
}

type errorResponse struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func (s *serverTestSuite) TestExchangeEndpointWrongVerifier() {
	t := s.Assert()

	authCode := "auth-code"
	accountID := "123"

	s.store.On("GetAuthorizationCode", authCode).Return(&AuthorizationCode{
		ID:                  "1",
		Code:                authCode,
		AccountID:           accountID,
		CodeChallenge:       "202CB962AC59075B964B07152D234B70",
		CodeChallengeMethod: "md5",
	}, nil).Once()
	accessToken := &AccessToken{
		ID:        "access-token",
		Code:      "test-access-token",
		AccountID: accountID,
		ExpiresAt: time.Now().UTC().Add(24 * time.Hour),
	}
	refreshToken := &RefreshToken{
		ID:            "refresh-token",
		Code:          "test-refresh-token",
		AccessTokenID: accessToken.ID,
	}
	s.store.On("ExchangeAuthorizationCode", mock.Anything).Return(accessToken, refreshToken, nil)

	s.server.setupExchangeEndpoint()

	jsonData := []byte(`{"code": "auth-code", "verifier": "1234"}`)

	res := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/v1/token", bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json")

	s.server.router.ServeHTTP(res, req)

	t.Equal(http.StatusBadRequest, res.Code)

	var data errorResponse
	err := json.Unmarshal(res.Body.Bytes(), &data)
	t.Nil(err)
	t.Equal(InvalidExchange, data.Code)
	t.Equal("verifier does not match", data.Message)
}

type loginEndpointTestCase struct {
	path     string
	expected string
}

func (s *serverTestSuite) TestLoginEndpoint() {
	t := s.Assert()

	s.server.EnableGoogle("123", "456")
	s.server.setupProviderEndpoints()

	testCases := []loginEndpointTestCase{
		{
			"/v1/auth/google",
			"https://accounts.google.com/o/oauth2/auth?client_id=123&redirect_uri=http%3A%2F%2Flocalhost.test%2Fv1%2Fauth%2Fgoogle%2Fcallback&response_type=code&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile&state=encrypted",
		},
	}

	s.randomizer.On("RandomString", 20).Return("random", nil).Times(len(testCases))
	s.crypto.On("Encrypt", s.server.config.StateSecret, mock.Anything).Return("encrypted", nil).Times(len(testCases))

	for _, testCase := range testCases {
		w := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", testCase.path, nil)
		s.server.router.ServeHTTP(w, req)

		t.Equal(302, w.Code)
		t.Equal(testCase.expected, w.Header().Get("location"))
	}

	s.randomizer.AssertExpectations(s.T())
	s.crypto.AssertExpectations(s.T())
}

func (s *serverTestSuite) sendCallbackRequest(providerName string, state *state) *httptest.ResponseRecorder {
	stateString, _ := json.Marshal(state)

	s.randomizer.On("RandomString", 20).Return(state.CSRF, nil).Once()
	s.crypto.On("Encrypt", s.server.config.StateSecret, mock.Anything).Return("encrypted", nil).Once()
	s.crypto.On("Decrypt", s.server.config.StateSecret, mock.Anything).Return(string(stateString), nil).Once()

	loginRes := httptest.NewRecorder()
	loginReq, _ := http.NewRequest("GET", fmt.Sprintf("/v1/auth/%s", providerName), nil)
	s.server.router.ServeHTTP(loginRes, loginReq)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/v1/auth/%s/callback", providerName), nil)
	for _, cookie := range loginRes.Result().Cookies() {
		req.AddCookie(cookie)
	}
	query := req.URL.Query()
	query.Set("state", "encrypted")
	req.URL.RawQuery = query.Encode()
	s.server.router.ServeHTTP(w, req)

	return w
}

func (s *serverTestSuite) TestGoogleCallbackEndpointWithNextURL() {
	t := s.Assert()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	s.server.EnableGoogle("123", "456")
	s.server.setupProviderEndpoints()

	st := &state{
		CSRF:            "random",
		Challenge:       "202CB962AC59075B964B07152D234B70", // 123
		ChallengeMethod: "md5",
		NextURL:         "http://localhost.test/auth/callback",
	}

	httpmock.RegisterResponder("POST", "https://accounts.google.com/o/oauth2/token",
		httpmock.NewStringResponder(200, `{"access_token": "access", "refresh_token": "refresh"}`))
	httpmock.RegisterResponder("GET", "https://www.googleapis.com/oauth2/v3/userinfo",
		httpmock.NewStringResponder(200, `{"sub": "123456", "email": "test@gmail.com", "name": "tester"}`))

	accountID := "123"
	s.store.On("UpsertAccount", "123456", Google, "test@gmail.com", "tester").Return(&Account{
		ID:         accountID,
		PlatformID: "123456",
		Platform:   Google,
		Name:       "tester",
		Email:      "test@gmail.com",
	}, nil).Twice()
	authCode := "test-auth-code"
	s.store.On("GenerateAuthorizationCode", accountID, st.Challenge, st.ChallengeMethod).Return(&AuthorizationCode{
		ID:                  "1",
		Code:                authCode,
		AccountID:           accountID,
		CodeChallenge:       st.Challenge,
		CodeChallengeMethod: st.ChallengeMethod,
	}, nil).Twice()

	// with next_url
	res := s.sendCallbackRequest(Google, st)

	t.Equal(302, res.Code)
	t.Equal(fmt.Sprintf("%s?code=%s", "http://localhost.test/auth/callback", authCode), res.Header().Get("location"))

	// when not having next_url, it sends JSON response
	st = &state{
		CSRF:            "random",
		Challenge:       "202CB962AC59075B964B07152D234B70", // 123
		ChallengeMethod: "md5",
	}
	res = s.sendCallbackRequest(Google, st)

	t.Equal(200, res.Code)
	var bodyMap map[string]string
	err := json.Unmarshal(res.Body.Bytes(), &bodyMap)
	t.Nil(err)
	t.Equal(authCode, bodyMap["code"])

	s.store.AssertExpectations(s.T())
}

func (s *serverTestSuite) TestMeEndpoint() {
	t := s.Assert()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	s.server.EnableGoogle("123", "456")
	s.server.setupMeEndpoint()

	res := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/me", nil)

	s.server.router.ServeHTTP(res, req)

	// no credentials
	t.Equal(401, res.Code)

	// with expired token
	expired, _ := s.server.signAccessToken(&AccessToken{
		ID:        "access-token",
		Code:      "expired-token",
		AccountID: "456",
		ExpiresAt: time.Now().UTC().Add(time.Duration(-1) * time.Hour),
	})

	res = httptest.NewRecorder()
	req.Header.Set("authorization", fmt.Sprintf("Bearer %s", expired))
	s.server.router.ServeHTTP(res, req)
	t.Equal(401, res.Code)

	// now with valid credentials
	accountID := "123"
	testAccount := &Account{
		ID:         accountID,
		PlatformID: "123456",
		Platform:   Google,
		Name:       "tester",
		Email:      "test@gmail.com",
	}
	s.store.On("GetAccountByID", accountID).Return(testAccount, nil).Once()

	bearer, _ := s.server.signAccessToken(&AccessToken{
		ID:        "access-token",
		Code:      "fake-access-token",
		AccountID: accountID,
		ExpiresAt: time.Now().UTC().Add(24 * time.Hour),
	})

	res = httptest.NewRecorder()
	req.Header.Set("authorization", fmt.Sprintf("Bearer %s", bearer))
	s.server.router.ServeHTTP(res, req)
	t.Equal(200, res.Code)
	var bodyMap map[string]string
	err := json.Unmarshal(res.Body.Bytes(), &bodyMap)
	t.Nil(err)
	t.Equal(testAccount.ID, bodyMap["id"])
	t.Equal(testAccount.PlatformID, bodyMap["platform_id"])
	t.Equal(testAccount.Platform, bodyMap["platform"])
	t.Equal(testAccount.Name, bodyMap["name"])
	t.Equal(testAccount.Email, bodyMap["email"])

	s.store.AssertExpectations(s.T())
}

func TestExampleTestSuite(t *testing.T) {
	suite.Run(t, new(serverTestSuite))
}
