package goauth

import (
	"encoding/json"
	"io/ioutil"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// NewGoogleProvider returns a GoogleProvider instance
func NewGoogleProvider(clientID, clientSecret, redirectURL string) *GoogleProvider {
	return &GoogleProvider{
		&oauth2.Config{
			ClientID:     clientID,
			ClientSecret: clientSecret,
			RedirectURL:  redirectURL,
			// these 2 scopes should provide enough info to construct an Account entry
			Scopes: []string{
				"https://www.googleapis.com/auth/userinfo.email",
				"https://www.googleapis.com/auth/userinfo.profile",
			},
			Endpoint: google.Endpoint,
		},
	}
}

// GoogleProvider implements Provider interface for Google OAuth2
type GoogleProvider struct {
	conf *oauth2.Config
}

// GetLoginURL returns the final auth URL to redirect the user to
// this depends on the specific OAuth provider
func (p *GoogleProvider) GetLoginURL(state string) string {
	return p.conf.AuthCodeURL(state)
}

type googleProfile struct {
	Sub   string `json:"sub"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

// GetAccount uses the (authorization) code to exchange for an access token and get the profile info
func (p *GoogleProvider) GetAccount(code string) (*Account, error) {
	token, err := p.conf.Exchange(oauth2.NoContext, code)
	if err != nil {
		return nil, err
	}

	client := p.conf.Client(oauth2.NoContext, token)

	res, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	data, _ := ioutil.ReadAll(res.Body)
	var profile googleProfile
	err = json.Unmarshal(data, &profile)
	if err != nil {
		return nil, err
	}

	return &Account{
		PlatformID: profile.Sub,
		Platform:   Google,
		Email:      profile.Email,
		Name:       profile.Name,
	}, nil
}
