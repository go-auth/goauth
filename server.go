package goauth

import (
	"context"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"gitlab.com/tanqhnguyen/gologger"
)

// Store defines how a Store implementation should behave
// A store is a mean to persist/retrieve data from a centralized data storage such as a database
type Store interface {
	// UpsertAccount inserts or updates an account. Accounts are unique based on their (platform, platformID)
	// Same email from 2 different platforms are valid
	UpsertAccount(
		platformID,
		platform,
		email,
		name string,
	) (*Account, error)

	// GenerateAuthorizationCode generates an authorization code associated with the challenge provided
	// This follows PKCE spec
	GenerateAuthorizationCode(accountID, challenge, challengeMethod string) (*AuthorizationCode, error)

	// GetAuthorizationCode returns an authorization code
	GetAuthorizationCode(code string) (*AuthorizationCode, error)

	// ExchangeAuthorizationCode exchanges the authorization code for an access and refresh token
	// This also marks the authoziation code as used and it can't be used again
	ExchangeAuthorizationCode(code string) (*AccessToken, *RefreshToken, error)

	// GetRefreshToken returns a refresh token
	GetRefreshToken(code string) (*RefreshToken, error)

	// RefreshAccessToken refreshes its associated access token
	RefreshAccessToken(refreshTokenCode string) (*AccessToken, error)

	// GetAccountByID returns the account entry based on the provided id
	GetAccountByID(id string) (*Account, error)

	// Close frees all resources
	Close(ctx context.Context) error
}

// Provider interface defines common methods to handle oauth2 flow
type Provider interface {
	// GetLoginURL returns the final auth URL to redirect the user to
	// this depends on the specific OAuth provider
	GetLoginURL(state string) string

	// GetAccount uses the (authorization) code to exchange for an access token and get the profile info
	GetAccount(code string) (*Account, error)
}

// Randomizer produces random values
type Randomizer interface {
	// RandomString returns a random string with the specified length
	RandomString(length int) (string, error)
}

// Crypto handles encrypt/decrypt logic
type Crypto interface {
	// Encrypt encrypts the input with the provided key
	Encrypt(key, input string) (string, error)
	// Decrypt decrypts the input with the provided key
	Decrypt(key, input string) (string, error)
}

var providers = map[string]Provider{}

// RegisterProvider registers a new provider
func RegisterProvider(name string, provider Provider) {
	providers[name] = provider
}

// Different providers
const (
	Google = "google"
)

type state struct {
	CSRF            string
	Challenge       string `form:"code_challenge"`
	ChallengeMethod string `form:"code_challenge_method" binding:"omitempty,oneof=md5 sha1 sha256"`
	NextURL         string `form:"next_url" binding:"omitempty,url"`
}

// Error codes
const (
	FailedToCreateCSRF       = "failed_to_create_csrf_token"
	FailedToSetupState       = "failed_to_setup_state"
	InvalidCSRFToken         = "invalid_csrf_token"
	FailedToGetAccount       = "failed_to_get_account_info"
	FailedToStoreAccount     = "failed_to_store_account"
	FailedToGenerateAuthCode = "failed_to_generate_auth_code"
	InvalidURL               = "invalid_url"
	InvalidExchange          = "invalid_exchange"
	InvalidRefresh           = "invalid_refresh"
	InvalidRequest           = "invalid_request"
	UnauthorizedRequest      = "unauthorized_request"
	UnexpectedError          = "unexpected_error"
)

// ServerConfig is the conf passed to the Server instance
type ServerConfig struct {
	RootURL              string
	AllowedRedirectHosts []string
	SessionSecret        string
	JWTSecret            string
	StateSecret          string
}

// Server handles auth requests
type Server struct {
	config     ServerConfig
	router     *gin.Engine
	store      Store
	logger     gologger.Logger
	randomizer Randomizer
	crypto     Crypto
}

// EnableGoogle enables Google OAuth2
func (s *Server) EnableGoogle(clientID, clientSecret string) {
	redirectURL := fmt.Sprintf("%s/v1/auth/%s/callback", s.config.RootURL, Google)
	RegisterProvider(Google, NewGoogleProvider(clientID, clientSecret, redirectURL))
}

func (s *Server) setupLoginEndpoint(name string, provider Provider) {
	s.router.GET(fmt.Sprintf("/v1/auth/%s", name), func(c *gin.Context) {
		session := sessions.Default(c)

		csrf, err := s.randomizer.RandomString(20)
		if err != nil {
			s.logger.Field("error", err).Error("failed to generate a random csrf token")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    FailedToCreateCSRF,
				"message": "failed to generate csrf token",
			})
			return
		}

		session.Set("csrf", csrf)
		session.Save()

		var state state
		if c.ShouldBindQuery(&state) != nil {
			s.logger.Field("error", err).Error("failed to extract data from qs to set up state")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    FailedToSetupState,
				"message": "failed to set up state from qs",
			})
			return
		}
		state.CSRF = csrf

		encrypted, err := s.encryptState(&state)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    FailedToSetupState,
				"message": "failed to encrypt state",
			})
			return
		}

		c.Redirect(302, provider.GetLoginURL(encrypted))
	})
}

func (s *Server) encryptState(state *state) (string, error) {
	d, err := json.Marshal(state)
	if err != nil {
		s.logger.Field("error", err).Error("failed to marshal state")
		return "", err
	}

	encrypted, err := s.crypto.Encrypt(s.config.StateSecret, string(d))
	if err != nil {
		s.logger.Field("error", err).Error("failed to encrypt state")
		return "", err
	}
	return encrypted, nil
}

func (s *Server) decryptState(input string) (*state, error) {
	decryptedState, err := s.crypto.Decrypt(s.config.StateSecret, input)

	if err != nil {
		return nil, err
	}

	var state state
	err = json.Unmarshal([]byte(decryptedState), &state)
	if err != nil {
		return nil, err
	}
	return &state, nil
}

func (s *Server) setupCallbackEndpoint(name string, provider Provider) {
	s.router.GET(fmt.Sprintf("/v1/auth/%s/callback", name), func(c *gin.Context) {
		session := sessions.Default(c)
		csrf := session.Get("csrf")

		state, err := s.decryptState(c.Query("state"))
		if err != nil {
			s.logger.Field("error", err).Error("failed to decrypt state")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    FailedToSetupState,
				"message": "invalid state",
			})
			return
		}

		csrfValue, ok := csrf.(string)
		if !ok {
			s.logger.Fields(gologger.Fields{
				"current": csrf,
			}).Error("failed to get current csrf value")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidCSRFToken,
				"message": "invalid token",
			})
			return
		}

		if state.CSRF != csrfValue {
			s.logger.Fields(gologger.Fields{
				"current":   csrfValue,
				"fromState": state.CSRF,
			}).Error("csrf token does not match")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidCSRFToken,
				"message": "invalid token",
			})
			return
		}

		account, err := provider.GetAccount(c.Query("code"))
		if err != nil {
			s.logger.Field("error", err).Error("failed to get account info from the provider")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    FailedToGetAccount,
				"message": "failed to get account info",
			})
			return
		}

		account, err = s.store.UpsertAccount(
			account.PlatformID,
			account.Platform,
			account.Email,
			account.Name,
		)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    FailedToStoreAccount,
				"message": "failed to store account",
			})
			return
		}

		authorizationCode, err := s.store.GenerateAuthorizationCode(
			account.ID,
			state.Challenge,
			state.ChallengeMethod,
		)

		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    FailedToGenerateAuthCode,
				"message": "failed to generate authorization code",
			})
			return
		}

		nextURL := state.NextURL

		if nextURL == "" {
			c.JSON(http.StatusOK, gin.H{
				"code": authorizationCode.Code,
			})
			return
		}

		urlObj, err := url.Parse(nextURL)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidURL,
				"message": "invalid next_url",
			})
			return
		}
		q := urlObj.Query()
		q.Set("code", authorizationCode.Code)
		urlObj.RawQuery = q.Encode()

		for _, allowedHost := range s.config.AllowedRedirectHosts {
			fmt.Println("urlObj.Host", urlObj.Host)
			if urlObj.Host == allowedHost {
				c.Redirect(302, urlObj.String())
				return
			}
		}

		s.logger.Field("next_url", nextURL).Error("next_url is not allowed")
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"code":    InvalidURL,
			"message": "invalid next_url",
		})
		return
	})
}

func (s *Server) unsignAccessToken(input string) (*AccessToken, error) {
	token, err := jwt.Parse(input, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(s.config.JWTSecret), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("invalid claims")
	}

	accountID, ok := claims["account_id"].(string)
	if !ok {
		return nil, errors.New("invalid account_id claim")
	}

	accessToken, ok := claims["access_token"].(string)
	if !ok {
		return nil, errors.New("invalid access_token claim")
	}

	expiresAt, ok := claims["expires_at"].(string)
	if !ok {
		return nil, errors.New("invalid expires_at claim")
	}

	expires, err := time.Parse(time.RFC3339, expiresAt)
	if err != nil {
		return nil, err
	}

	return &AccessToken{
		AccountID: accountID,
		Code:      accessToken,
		ExpiresAt: expires.UTC(),
	}, nil
}

func (s *Server) signAccessToken(accessToken *AccessToken) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"account_id":   accessToken.AccountID,
		"access_token": accessToken.Code,
		"expires_at":   accessToken.ExpiresAt.Format(time.RFC3339),
	})
	return token.SignedString([]byte(s.config.JWTSecret))
}

var codeVerifiers = map[string]func(verifier, challenge string) bool{
	"md5": func(verifier, challenge string) bool {
		hash := fmt.Sprintf("%x", md5.Sum([]byte(verifier)))
		return hash == strings.ToLower(challenge)
	},
	"sha1": func(verifier, challenge string) bool {
		hash := fmt.Sprintf("%x", sha1.Sum([]byte(verifier)))
		return hash == challenge
	},
	"sha256": func(verifier, challenge string) bool {
		hash := fmt.Sprintf("%x", sha256.Sum256([]byte(verifier)))
		return hash == challenge
	},
}

type exchangeRequest struct {
	Code     string `form:"code" json:"code" binding:"required,min=1"`
	Verifier string `form:"verifier" json:"verifier" binding:"required,min=1"`
}

func (s *Server) setupExchangeEndpoint() {
	s.router.POST("/v1/token", func(c *gin.Context) {
		var req exchangeRequest
		if err := c.ShouldBind(&req); err != nil {
			s.logger.Field("error", err).Error("failed to bind exchange request")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidExchange,
				"message": "failed to parse exchange data",
			})
			return
		}

		authCode, err := s.store.GetAuthorizationCode(req.Code)
		if err != nil || authCode == nil {
			s.logger.Field("error", err).Error("failed to get auth code")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidExchange,
				"message": "invalid code",
			})
			return
		}

		if authCode.ConvertedToToken != "" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidExchange,
				"message": "invalid code",
			})
			return
		}

		verify, ok := codeVerifiers[authCode.CodeChallengeMethod]
		if !ok {
			s.logger.Field("method", authCode.CodeChallengeMethod).Error("invalid challenge method")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidExchange,
				"message": "invalid method",
			})
			return
		}

		if !verify(req.Verifier, authCode.CodeChallenge) {
			s.logger.Fields(gologger.Fields{
				"id":       authCode.ID,
				"verifier": req.Verifier,
			}).Error("failed to match verifier against challenge")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidExchange,
				"message": "verifier does not match",
			})
			return
		}

		accessToken, refreshToken, err := s.store.ExchangeAuthorizationCode(authCode.Code)
		if err != nil {
			s.logger.Field("error", err).Error("failed to exchange auth code")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidExchange,
				"message": "unable to exchange for access token",
			})
			return
		}

		tokenString, err := s.signAccessToken(accessToken)
		if err != nil {
			s.logger.Field("error", err).Error("failed to sign jwt")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidExchange,
				"message": "unable to sign token",
			})
			return
		}

		now := time.Now().UTC()
		diff := accessToken.ExpiresAt.Sub(now)

		c.JSON(http.StatusOK, gin.H{
			"access_token":  tokenString,
			"refresh_token": refreshToken.Code,
			"token_type":    "Bearer",
			"expires_in":    int(diff.Seconds()),
		})
	})
}

type refreshRequest struct {
	RefreshToken string `form:"refresh_token" json:"refresh_token" binding:"required,min=1"`
}

func (s *Server) setupRefreshEndpoint() {
	s.router.PUT("/v1/token", func(c *gin.Context) {
		var req refreshRequest

		if err := c.ShouldBind(&req); err != nil {
			s.logger.Field("error", err).Error("failed to bind refresh request")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidRefresh,
				"message": "failed to parse refresh data",
			})
			return
		}

		refreshToken, err := s.store.GetRefreshToken(req.RefreshToken)
		if err != nil || refreshToken == nil {
			s.logger.Field("error", err).Error("failed to get refresh token")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidRefresh,
				"message": "invalid refresh token",
			})
			return
		}

		accessToken, err := s.store.RefreshAccessToken(refreshToken.Code)
		if err != nil {
			s.logger.Field("error", err).Error("failed to refresh access token")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidRefresh,
				"message": "invalid refresh token",
			})
			return
		}

		tokenString, err := s.signAccessToken(accessToken)
		if err != nil {
			s.logger.Field("error", err).Error("failed to sign jwt")
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidRefresh,
				"message": "unable to sign token",
			})
			return
		}

		now := time.Now().UTC()
		diff := accessToken.ExpiresAt.Sub(now)

		c.JSON(http.StatusOK, gin.H{
			"access_token":  tokenString,
			"refresh_token": "",
			"token_type":    "Bearer",
			"expires_in":    int(diff.Seconds()),
		})
	})
}

// SetStore defines which store implementation to use
func (s *Server) SetStore(store Store) {
	s.store = store
}

func (s *Server) setupProviderEndpoints() {
	if len(providers) == 0 {
		panic("no providers found")
	}

	for name, provider := range providers {
		s.setupLoginEndpoint(name, provider)
		s.setupCallbackEndpoint(name, provider)
	}
}

func (s *Server) verifyAccessToken(c *gin.Context) {
	authorization := c.Request.Header.Get("authorization")

	if authorization == "" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"code":    UnauthorizedRequest,
			"message": "unauthorized request",
		})
		return
	}

	parts := strings.Split(authorization, "Bearer")
	if len(parts) != 2 {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"code":    UnauthorizedRequest,
			"message": "unauthorized request",
		})
		return
	}

	code := strings.TrimSpace(parts[1])
	accessToken, err := s.unsignAccessToken(code)
	now := time.Now().UTC()

	if err != nil || accessToken.ExpiresAt.Before(now) {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"code":    UnauthorizedRequest,
			"message": "unauthorized request",
		})
		return
	}

	c.Set("accessToken", accessToken)

	c.Next()
}

func (s *Server) setupMeEndpoint() {
	s.router.GET("/v1/me", s.verifyAccessToken, func(c *gin.Context) {
		accessTokenValue, ok := c.Get("accessToken")
		if !ok {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidRequest,
				"message": "invalid request",
			})
			return
		}

		accessToken, ok := accessTokenValue.(*AccessToken)
		if !ok {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"code":    InvalidRequest,
				"message": "invalid request",
			})
			return
		}

		account, err := s.store.GetAccountByID(accessToken.AccountID)
		if err != nil {
			s.logger.Field("error", err).Error("failed to get account")
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"code":    UnexpectedError,
				"message": "failed to get account",
			})
			return
		}

		c.JSON(http.StatusOK, account)
	})
}

// Start starts the server and handles graceful shutdown
func (s *Server) Start(address string) {
	s.setupProviderEndpoints()

	if s.store == nil {
		panic("must call SetStore first and provide a valid Store implementation")
	}

	s.setupExchangeEndpoint()
	s.setupRefreshEndpoint()
	s.setupMeEndpoint()

	srv := &http.Server{
		Addr:    address,
		Handler: s.router,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			s.logger.Field("error", err).Error("failed to start")
		}
	}()

	exit := make(chan os.Signal)
	signal.Notify(exit, syscall.SIGINT, syscall.SIGTERM)
	<-exit

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		s.logger.Field("error", err).Error("failed to shutdown")
	}

	if err := s.store.Close(ctx); err != nil {
		s.logger.Field("error", err).Error("failed to close store")
	}

	s.logger.Info("exiting...")
}

// NewServer returns a new Server instance based on the provided config
func NewServer(config ServerConfig) *Server {
	router := gin.Default()

	store := cookie.NewStore([]byte(config.SessionSecret))

	router.Use(sessions.Sessions("goauth", store))

	return &Server{
		config,
		router,
		nil,
		gologger.NewLogrusLoggerFromEnvironment(),
		new(DefaultRandomizer),
		new(DefaultCrypto),
	}
}
