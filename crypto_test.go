package goauth

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type cryptoTestSuite struct {
	suite.Suite
	crypto *DefaultCrypto
}

func (s *cryptoTestSuite) SetupTest() {
	s.crypto = new(DefaultCrypto)
}

func (s *cryptoTestSuite) TestEncryptionLogic() {
	t := s.Assert()

	original := "test"

	key := "12345678901234567890123456789012"
	encrypted, err := s.crypto.Encrypt(key, original)
	t.Nil(err)

	decrypted, err := s.crypto.Decrypt(key, encrypted)
	t.Nil(err)

	t.Equal(original, decrypted)
}

func TestCryptoSuite(t *testing.T) {
	suite.Run(t, new(cryptoTestSuite))
}
